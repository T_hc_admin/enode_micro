using ECommon.Components;
using ENode.EQueue;
using ENode.Eventing;
using ENodeMicro.Domain.Members.Events;

namespace ENodeMicro.CommandHost.TopicProviders
{
    [Component]
    public class EventTopicProvider : AbstractTopicProvider<IDomainEvent>
    {
        public EventTopicProvider()
        {
            RegisterTopic("MemberEventTopic", typeof(MemberCreatedEvent));
        }
    }
}