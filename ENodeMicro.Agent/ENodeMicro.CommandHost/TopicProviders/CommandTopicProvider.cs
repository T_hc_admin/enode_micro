using ECommon.Components;
using ENode.Commanding;
using ENode.EQueue;
using ENodeMicro.Commands.Managers;

namespace ENodeMicro.CommandHost.TopicProviders
{
    [Component]
    public class CommandTopicProvider : AbstractTopicProvider<ICommand>
    {
        public CommandTopicProvider()
        {
            RegisterTopic("ManagerCommandTopic", typeof(CreateManagerCommand));
        }
    }
}