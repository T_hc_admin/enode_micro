using System;
using System.Threading.Tasks;
using ECommon.Components;

namespace ENodeMicro.Domain.Members.Services
{
    /// <summary>
    /// 会员领域服务
    /// </summary>
    [Component]
    public class MemberService
    {
        /// <summary>
        /// 检查手机号是否重复
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <returns></returns>
        /// <exception cref="NullReferenceException"></exception>
        public Task CheckPhoneNumberForCreate(string phoneNumber)
        {
            // throw new NullReferenceException();
            return Task.CompletedTask;
        }
    }
}