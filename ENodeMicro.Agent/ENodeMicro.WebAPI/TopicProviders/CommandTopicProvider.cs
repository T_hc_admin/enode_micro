using ECommon.Components;
using ENode.Commanding;
using ENode.EQueue;

namespace ENodeMicro.WebAPI.TopicProviders
{
    [Component]
    public class CommandTopicProvider : AbstractTopicProvider<ICommand>
    {
        public CommandTopicProvider()
        {
            // RegisterTopic("MemberCommandTopic", typeof(CreateMemberWithPhoneNumberCommand));
        }
    }
}