using System;
using System.Threading.Tasks;
using ECommon.Utilities;
using ENode.Commanding;
using ENode.Messaging;
using ENodeMicro.Commands.Managers;
using ENodeMicro.Messages.Members;

namespace ENodeMicro.ProcessManagers
{
    /// <summary>
    /// 经营者流程管理器
    /// </summary>
    public class ManagerProcessManager
        : IMessageHandler<MemberCreatedMessage>
    {
        private readonly ICommandService _commandService;

        public ManagerProcessManager(ICommandService commandService)
        {
            _commandService = commandService;
        }

        /// <summary>
        /// 会员创建成功消息
        ///     TODO 测试不同BC通讯
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public Task HandleAsync(MemberCreatedMessage message)
        {
            return _commandService.SendAsync(new CreateManagerCommand(
                ObjectId.GenerateNewStringId(),
                message.Nickname,
                message.PhoneNumber));
        }
    }
}
