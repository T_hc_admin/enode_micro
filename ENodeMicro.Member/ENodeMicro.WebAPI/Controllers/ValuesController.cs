using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime;
using ENodeMicro.GRPC;
using ENodeMicro.QueryServices.Interfaces.Members;
using Microsoft.AspNetCore.Mvc;

namespace ENodeMicro.WebAPI.Controllers
{
    [Route("MemberModuleHttpService/[controller]")]
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly IMemberQueryService _memberQueryService;
        private readonly Manager.ManagerClient _client;

        public ValuesController(IMemberQueryService memberQueryService)
        {
            _memberQueryService = memberQueryService;
            var channel = GRpcClient.GetGRpcChannel(ServiceName.AgentModuleGRpcService);
            _client = channel.GetInstance<Manager.ManagerClient>();
        }
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            var rsp = _memberQueryService.GetMembersAsync().Result;
            var managerRes = _client.GetManager(new GetManagerRequest
            {
                Id = "5f05eb00d74a4657a2a12468"
            });
            return new string[] { "value1", "value2",rsp.Message,managerRes.PhoneNumber };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        [HttpGet("info")]
        public string GetServerInfo()
        {
            var p = Process.GetProcesses();//获取进程信息

            long workingSet64 = 0;
            var info = "";
            foreach (var pr in p)
            {
                workingSet64 += pr.WorkingSet64 / 1024 / 1024;
                info += pr.ProcessName + "内存WorkingSet64：-----------" + (pr.WorkingSet64 / 1024 / 1024).ToString() + "M\r\n";
            }
            info = "-------------------------------------------------------------------------------------\r\n" + info;//得到进程内存
            info = "内存WorkingSet64：-----------" + (workingSet64).ToString() + "M\r\n" + info;//得到进程内存
            info = "是否为ServerGC：-----------" + (GCSettings.IsServerGC).ToString() + "\r\n" + info;//得到进程内存
            info = "-------------------------------总计--------------------------------------------------\r\n" + info;//得到进程内存

            return info;
        }
    }
}