using System;
using ENodeMicro.Common.Enums;

namespace ENodeMicro.QueryServices.Models.Managers
{
    /// <summary>
    /// 经营者
    /// </summary>
    public class Manager : ModelBase
    {
        /// <summary>
        /// 姓名
        /// </summary>
        /// <value></value>
        public string Name { get; set; }
        /// <summary>
        /// 手机号
        /// </summary>
        /// <value></value>
        public string PhoneNumber { get; set; }
    }
}