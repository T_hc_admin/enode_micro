﻿using System.Net;
using System.Runtime.InteropServices;
using ECommon.Components;
using ECommon.Configurations;
using ECommon.Logging;
using ECommon.Serilog;
using ECommon.Socketing;
using ENodeMicro.Infrastructure;
using EQueue.Configurations;
using EQueue.NameServer;
using ECommonConfiguration = ECommon.Configurations.Configuration;

namespace ENodeMicro.NameServerHost
{
    public class Bootstrap
    {
        private static ECommonConfiguration _ecommonConfiguration;
        private static NameServerController _nameServer;

        public static void Initialize()
        {
            InitializeEQueue();
        }
        public static void Start()
        {
            _nameServer.Start();
        }
        public static void Stop()
        {
            if (_nameServer != null)
            {
                _nameServer.Shutdown();
            }
        }

        private static void InitializeEQueue()
        {
            var loggerFactory = new SerilogLoggerFactory();
            //.AddFileLogger("ECommon", "logs\\ecommon")
            //.AddFileLogger("EQueue", "logs\\equeue")
            //.AddFileLogger("ENode", "logs\\enode");

            _ecommonConfiguration = ECommonConfiguration
                .Create()
                .UseAutofac()
                .RegisterCommonComponents()
                .UseSerilog(loggerFactory)
                .UseJsonNet()
                .RegisterUnhandledExceptionHandler()
                .RegisterEQueueComponents()
                .BuildContainer();

            var address = RuntimeInformation.IsOSPlatform(OSPlatform.Linux) ? SocketUtils.GetLocalIPV4() : IPAddress.Loopback;
            var setting = new NameServerSetting()
            {
                BindingAddress = new IPEndPoint(address, ConfigSettings.NameServerPort)
            };
            _nameServer = new NameServerController(setting);
            ObjectContainer.Resolve<ILoggerFactory>().Create(typeof(Bootstrap).FullName).Info("NameServer initialized.");
        }
    }
}
