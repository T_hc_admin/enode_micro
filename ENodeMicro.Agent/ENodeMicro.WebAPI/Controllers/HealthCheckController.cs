using Microsoft.AspNetCore.Mvc;

namespace ENodeMicro.WebAPI.Controllers
{
    [Route("health_check")]
    [ApiController]
    public class HealthCheckController : ControllerBase
    {
        // GET
        public IActionResult Get()
        {
            return Content("check passing");
        }
    }
}