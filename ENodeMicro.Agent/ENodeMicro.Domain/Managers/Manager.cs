using System;
using ENode.Domain;
using ENodeMicro.Common.Enums;
using ENodeMicro.Domain.Managers.Events;

namespace ENodeMicro.Domain.Managers
{
    /// <summary>
    /// 经营者聚合根
    /// </summary>
    public class Manager : AggregateRoot<string>
    {
        /// <summary>
        /// 姓名
        /// </summary>
        /// <value></value>
        public string Name { get; private set; }
        /// <summary>
        /// 手机号
        /// </summary>
        /// <value></value>
        public string PhoneNumber { get; private set; }
        private Manager() { }

        // 创建
        public Manager(string id, string name, string phoneNumber)
            : base(id)
        {
            // 发布事件
            ApplyEvent(new ManagerCreatedEvent(name, phoneNumber));
        }

        #region Private handles

        // 创建
        private void Handle(ManagerCreatedEvent @event)
        {
            Name = @event.Name;
            PhoneNumber = @event.PhoneNumber;
        }
        #endregion
    }
}