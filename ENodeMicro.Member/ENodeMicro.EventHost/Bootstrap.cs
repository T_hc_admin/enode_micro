using System.Configuration;
using System.Reflection;
using ECommon.Components;
using ECommon.Configurations;
using ECommon.Logging;
using ECommon.Serilog;
using ENode.Configurations;
using ENode.MySQL;
using ENodeMicro.Common;

namespace ENodeMicro.EventHost
{
    public class Bootstrap
    {
        private static ENodeConfiguration _enodeConfiguration;

        public static void Initialize()
        {
            InitializeENode();
        }
        public static void Start()
        {
            _enodeConfiguration.StartEQueue().Start();
        }
        public static void Stop()
        {
            _enodeConfiguration.ShutdownEQueue().Stop();
        }

        private static void InitializeENode()
        {
            var assemblies = new[]
            {
                Assembly.Load("ENodeMicro.Common"),
                Assembly.Load("ENodeMicro.Domain"),
                Assembly.Load("ENodeMicro.Denormalizers.Dapper"),
                Assembly.Load("ENodeMicro.EventHost")
            };

            var loggerFactory = new SerilogLoggerFactory();
            // .AddFileLogger("ECommon", "logs\\ecommon")
            // .AddFileLogger("EQueue", "logs\\equeue")
            // .AddFileLogger("ENode", "logs\\enode", minimumLevel: Serilog.Events.LogEventLevel.Debug);

            _enodeConfiguration = ECommon.Configurations.Configuration
                .Create()
                .UseAutofac()
                .RegisterCommonComponents()
                .UseSerilog(loggerFactory)
                .UseJsonNet()
                .RegisterUnhandledExceptionHandler()
                .CreateENode()
                .RegisterENodeComponents()
                .RegisterBusinessComponents(assemblies)
                .UseMySqlPublishedVersionStore()
                .UseEQueue()
                .BuildContainer()
                .InitializeMySqlPublishedVersionStore(ConfigSettings.ENodeConnectionString)
                .InitializeBusinessAssemblies(assemblies);

            ObjectContainer.Resolve<ILoggerFactory>().Create(typeof(Program)).Info("Event Host initialized.");
        }
    }
}