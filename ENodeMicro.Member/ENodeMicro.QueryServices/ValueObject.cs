using System;
using System.ComponentModel.DataAnnotations;

namespace ENodeMicro.QueryServices
{
    /// <summary>
    /// 值对象需要继承的类
    /// </summary>
    public class ValueObject
    {
        /// <summary>
        /// ID自增
        /// </summary>
        [Key]
        public int Id { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreatedOn { get; set; }
    }
}